import sys

import tanjun
import logging
log = logging.getLogger(name=__name__)

component = tanjun.Component(name=__name__)
group_admin = component.with_slash_command(tanjun.slash_command_group("admin", "Admin commands for the bot"))


@group_admin.with_command
@tanjun.with_owner_check()
@tanjun.with_str_slash_option("module_name", "Target module")
@tanjun.as_slash_command("reload_module", "Reloads a module", default_to_ephemeral=True)
async def reload_module(ctx: tanjun.abc.SlashContext,
                        module_name: str,
                        client: tanjun.Client = tanjun.injected(type=tanjun.Client)):
    try:
        client.reload_modules(module_name)
    except ValueError:
        client.load_modules(module_name)
    except tanjun.errors.FailedModuleUnload:
        await ctx.respond("Could not unload module - check console for details")
        log.exception("Error unloading module")
        return

    await client.declare_global_commands()
    await ctx.respond("Reloaded")


@group_admin.with_command
@tanjun.with_owner_check()
@tanjun.with_str_slash_option("module_name", "Target module")
@tanjun.as_slash_command("unload_module", "Unloads a module", default_to_ephemeral=True)
async def unload_module(ctx: tanjun.abc.SlashContext,
                        module_name: str,
                        client: tanjun.Client = tanjun.injected(type=tanjun.Client)):
    try:
        client.unload_modules(module_name)
    except ValueError:
        await ctx.respond("Couldn't unload module (not found?)")

    await client.declare_global_commands()
    await ctx.respond("Unloaded")


@group_admin.with_command
@tanjun.with_owner_check()
@tanjun.with_str_slash_option("module_name", "Target module")
@tanjun.as_slash_command("load_module", "Reloads a module", default_to_ephemeral=True)
async def load_module(ctx: tanjun.abc.SlashContext,
                      module_name: str,
                      client: tanjun.Client = tanjun.injected(type=tanjun.Client)):
    try:
        client.load_modules(module_name)
    except ValueError:
        await ctx.respond("Couldn't load module (not found?)")
        return
    await client.declare_global_commands()
    await ctx.respond("Loaded")


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    client.add_component(component.copy())

@tanjun.as_unloader
def unload(client: tanjun.abc.Client) -> None:
    client.remove_component(component.copy())
