import typing

import tanjun
import logging
import pytz
log = logging.getLogger(name=__name__)

component = tanjun.Component()

group_time = component.with_slash_command(tanjun.slash_command_group("time", "See what time it is for others and "
                                                                             "manage timezones"))
group_time_tz = group_time.with_command(tanjun.slash_command_group("tz", "See other people's timezones or set your "
                                                                         "own"))


@group_time_tz.with_command
@tanjun.with_str_slash_option("timezone", "Timezone you're in - omit this to see examples", default=None)
@tanjun.as_slash_command("set", "Set your timezone. Run without arguments to see examples", default_to_ephemeral=True)
async def timezone_set(ctx: tanjun.abc.Context, timezone: typing.Optional[str]) -> None:
    if timezone is None:
        resp_text = """
            **You can use any valid timezone specification for pytz here. Some examples:**
            `US/Eastern`
            `America/New_York`
            `UTC`
            `Etc/UTC`
            `Etc/GMT`
            `Etc/GMT0`
            `Etc/GMT-5`
        """
        await ctx.respond("\n".join([text.lstrip(' ') for text in resp_text.split('\n')]))
    pass


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    client.add_component(component.copy())
