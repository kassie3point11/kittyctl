import os
from .bot import KittyctlBot


bot = KittyctlBot(token=os.environ["TOKEN"])
bot.run()
