import hikari
import tanjun


class KittyctlBot:
    def __init__(self, token):
        self.bot = hikari.GatewayBot(token=token)
        self.client = tanjun.Client.from_gateway_bot(self.bot, declare_global_commands=955199164344565830,
                                                     mention_prefix=True)
        self.client.load_modules("kittyctl.modules.time")
        self.client.load_modules("kittyctl.modules.admin")

    def run(self):
        self.bot.run()
